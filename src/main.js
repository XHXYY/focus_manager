import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router';
import './assets/index.css'
import manager from './components/manager.vue'
import manager_all from './components/manager_all.vue'
import content from './components/content.vue'
import login from './components/login.vue'


Vue.use(VueAxios, axios)
Vue.use(VueRouter)
Vue.config.productionTip = false

Vue.component('manager', manager)
Vue.component('manager_all', manager_all)
Vue.component('content', content)
Vue.component('login', login)


const routes = [
  {
    path: '/', component: manager, name:"manager",meta: {
      // 页面标题title
      title: '预约管理'
    }
  },
  {
    path: '/content', component: content, name:"content",meta: {
      // 页面标题title
      title: '预约管理'
    }
  },
  {
    path: '/manager_all', component: manager_all, name:"manager_all",meta: {
      // 页面标题title
      title: '预约管理'
    }
  },
  {
    path: '/login', component: login, name:"login",meta: {
      // 页面标题title
      title: '预约管理'
    }
  },
]

const router = new VueRouter({
  routes // (缩写) 相当于 routes: routes
})



new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')