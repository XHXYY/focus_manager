import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify);

export default new Vuetify({
    theme:{
        dark:true,
        themes: {
            dark: {
              primary: colors.red.darken2, // #E53935
              secondary: colors.red.darken4, // #FFCDD2
              accent: colors.red.lighter1, // #3F51B5
            },
          },
    },
});